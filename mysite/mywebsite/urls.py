from django.contrib import admin
from django.urls import path,include
from  mywebsite import views

urlpatterns = [
    path('',views.hello, name="hellopage"),
    path("mysite/hello",views.hello),
     path("mysite/ece",views.ece),
     path("mysite/eee",views.eee),
]
